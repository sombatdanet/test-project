import 'package:flutter/material.dart';
import 'package:flutter_omkousol/page/page_movie.dart';

import 'List.dart';
import 'code/ListViewMovie.dart';

class Homepage extends StatelessWidget {
  final List<MovieList> storymovie;
  const Homepage({super.key, required this.storymovie});

  @override
  Widget build(BuildContext context) {
    var url =
        "https://i.pinimg.com/564x/bd/5d/ec/bd5decd9383865073c3b58674b647782.jpg";
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.menu,
                      color: Colors.black,
                    ),
                    Icon(
                      Icons.search,
                      color: Colors.black,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Hi Jane ",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                          Text(
                            "See what's new today ",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        child: CircleAvatar(
                      backgroundImage: NetworkImage(url),
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                child: Text(
                  "Latest",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                    itemCount:storymovie.length,itemBuilder: (context, index) {
                  return MoviePageView(storymovie: storymovie[index],);
                }),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20,vertical:0),
                child: Text(
                  "Favorite",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
              Container(
                height:400,
                child: ListView.builder(
                  itemCount: storymovie.length,
                  itemBuilder: (context,index){
                    return ListViewMovie(storylist:storymovie[index]);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
