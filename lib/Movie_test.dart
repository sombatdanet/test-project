import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ImageDetail.dart';
import 'List.dart';

class Movienew extends StatefulWidget {
  final MovieModel movie;
  final NetFlix netflix;
  Movienew({required this.movie, required this.netflix});
  @override
  State<Movienew> createState() => _MovienewState();
}

class _MovienewState extends State<Movienew> {
  @override
  Widget build(BuildContext context) {
    return   InkWell(
      onTap: (){
        Navigator.push((context), MaterialPageRoute(builder: (context)=>ImageDetail(image: widget.movie, netflix: widget.netflix,)));
      },
      child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(left: 5),
              height: 150,
              width: 100,
              child:CachedNetworkImage(
                imageUrl: widget.movie.image,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                        colorFilter:
                        ColorFilter.mode(Colors.red.withOpacity(0.1), BlendMode.colorBurn)),
                  ),
                ),
                placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            _buildButton,
          ]),
    );
  }
  Widget get _buildButton {
    return Container(
      child: IconButton(onPressed: (){
      },icon: Icon(Icons.play_circle_outline_outlined,size: 50,color: Colors.white,),),
    );
  }
}
