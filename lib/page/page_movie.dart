import 'package:flutter/material.dart';

import '../List.dart';

class MoviePageView extends StatefulWidget {
  final MovieList storymovie;
  const MoviePageView({super.key, required this.storymovie});

  @override
  State<MoviePageView> createState() => _MoviePageViewState();
}

class _MoviePageViewState extends State<MoviePageView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              width: MediaQuery.of(context).size.width * 0.3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    height: MediaQuery.of(context).size.height* 0.3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image:AssetImage(widget.storymovie.imageurl)
                      )
                    ),
                  ),SizedBox(height: 10,),
                  Text(widget.storymovie.moviename,style: TextStyle(fontSize:15,fontWeight: FontWeight.bold),),
                  Text(widget.storymovie.date,style: TextStyle(fontSize: 12),),
                ],
              )),
        ],
      ),
    );
  }
}
