import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'ChangeNotfier.dart';
import 'List.dart';

class ImageDetail extends StatefulWidget {
  final MovieModel image;
  final NetFlix netflix;
  ImageDetail({super.key, required this.image, required this.netflix});

  @override
  State<ImageDetail> createState() => _ImageDetailState();
}

class _ImageDetailState extends State<ImageDetail> {
  @override
  Widget build(BuildContext context) {
    bool inFavorite = context.watch<TextChange>().isFavorite(widget.netflix);
    return Provider (
      create:(context)=>TextChange(),
      child: MaterialApp(
        home:Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(onPressed: (){
                Navigator.push((context), MaterialPageRoute(builder: (context)=>_buildFavortie));
              }, icon: Icon(Icons.favorite))
            ],
              leading: IconButton(onPressed: (){} ,icon: Icon(Icons.menu,color: Colors.white,),),
              centerTitle: true,
              backgroundColor: Colors.black,
              title: Text("NETFLIX",
                  style: TextStyle(
                    color: Colors.red[900],
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1,
                  ))),
          body: Center(
            child: Stack(
              children: [
                Container(
                  height: 400,
                  width: 400,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(widget.image.image),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 20,
                    left: 20,
                    child: IconButton(onPressed: (){
                    !inFavorite?context.read<TextChange>().addFavorite(widget.netflix):context.read<TextChange>().removeFavorite(widget.netflix);
                    },icon: Icon(Icons.favorite,color: inFavorite?Colors.red:Colors.white,
                    ),))
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget get _buildFavortie{
    List<NetFlix> favorite = context.watch<TextChange>().listSave;
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(onPressed: (){} ,icon: Icon(Icons.menu,color: Colors.white,),),
          centerTitle: true,
          backgroundColor: Colors.black,
          title: Text("FAVORITE",
              style: TextStyle(
                color: Colors.red[900],
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ))),
      body:Container(
        child: GridView.extent(
          maxCrossAxisExtent: 200,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          children: [
            for(int i =0;i<favorite.length;i++)
               _buildItem(favorite[i]),
          ],
        ),
      ),
    );
  }
  Widget  _buildItem(NetFlix item){
    return Stack(
      children: [
        Container(
          height: 500,
          width: 500,
          child: CachedNetworkImage(
            imageUrl: item.url,
            placeholder: (context,url)=>Container(color: Colors.grey,),
            errorWidget: (context,url,erro)=>Container(color: Colors.black,),
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          bottom:10,
          right: 20, child: (Container
          (child:IconButton(onPressed: (){
            context.read<TextChange>().removeFavorite(item);
        },
        icon: Icon(Icons.remove_circle_outlined,color: Colors.white,size: 50,),),)),
        ),
      ],
    );
  }
}