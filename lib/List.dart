class MovieModel {
  String image;
  MovieModel({required this.image});
}

List<MovieModel> movielist = [
  MovieModel(
    image:
        "https://i.pinimg.com/564x/f2/a2/55/f2a2554cf70e91bbdd8309128a817d97.jpg",
  ),
  MovieModel(
    image:
        "https://i.pinimg.com/236x/0d/9e/bd/0d9ebde77b69855af39c302abcea7c97.jpg",
  ),
  MovieModel(
    image:
        "https://i.pinimg.com/236x/0b/9b/1a/0b9b1a6a219de5a55dfafffff3e36846.jpg",
  ),
  MovieModel(
    image:
        "https://i.pinimg.com/236x/dc/25/5c/dc255c2963040e1fc97b1b84676a60cf.jpg",
  ),
  MovieModel(
    image:
        "https://i.pinimg.com/236x/e2/df/b0/e2dfb0f27f3b6ec4b56ae1aa56b0f86e.jpg",
  ),
  MovieModel(
    image:
        "https://i.pinimg.com/236x/6d/b7/11/6db7112503ae36098653ab3e361d9785.jpg",
  ),
];

class NetFlix {
  int id;
  String url;
  String title;
  String subtitle;
  NetFlix(
      {required this.id,
      required this.url,
      required this.title,
      required this.subtitle});
}

List<NetFlix> mnetlifx = [
  NetFlix(
    id: 1,
    url:
        "https://i.pinimg.com/564x/21/59/62/215962edb2bc66e4f30bf41e3e1092a1.jpg",
    title: "AVENGER",
    subtitle: "AVENGER *BEST ACTION AND EMOTION FOR EVERYONE",
  ),
  NetFlix(
    id: 2,
    url:
        "https://i.pinimg.com/236x/5b/7e/50/5b7e50df1011eb2cac54c609e0e479f3.jpg",
    title: "MOON NIGHT",
    subtitle: "Moon Night *BEST ACTION AND FIGHTING FOR EVERYONE",
  ),
  NetFlix(
    id: 3,
    url:
        "https://i.pinimg.com/736x/f6/e2/91/f6e2919028fe67d4ba81f1243049f68a.jpg",
    title: "JONH WICK",
    subtitle: "JONH WICK *BEST ACTION AND FIGHTING FOR EVERYONE",
  ),
  NetFlix(
    id: 4,
    url:
        "https://i.pinimg.com/564x/4b/86/30/4b86304218c917c53c2566dd9a631676.jpg",
    title: "DOCTOR STRANGE",
    subtitle: " DOCTOR STRANG *BEST MAGIC AND  FOR EVERYONE",
  ),
];

class ListCast {
  List<String> namecast;
  List<String> imgcast;
  ListCast({required this.imgcast, required this.namecast});
}

List<ListCast> listcast =[
  ListCast( namecast: [
    "Charlie Cox",
    "Deborah Ann Woll",
    "Vincent D'Onofrio",
    "elden henson",
  ],
    imgcast: [
      "https://i.pinimg.com/564x/86/88/6b/86886b7e2a712f9c837724ddeb66a404.jpg",
      "https://i.pinimg.com/236x/f0/14/27/f014271fa9e28ecd9ddbbcf40b2d0fc1.jpg",
      "https://i.pinimg.com/236x/f6/5c/b0/f65cb03dc9b4063ff77dd994c9464420.jpg",
      "https://i.pinimg.com/236x/ce/0f/db/ce0fdb48c73080803c7942c0ee49bb39.jpg",
    ],),
  ListCast( namecast: [
    "Nicolas Cage",
    "Eva Mendes",
    "Wes Bentley",
    "sam elliott",
  ],
    imgcast: [
      "https://i.pinimg.com/236x/c6/05/12/c6051284c26490320a9c7751dfcc5a43.jpg",
      "https://i.pinimg.com/236x/53/15/8b/53158b63a2f65c63db7f4ec26c958df0.jpg",
      "https://i.pinimg.com/236x/1c/26/ed/1c26ed82430c7b94b82539f4a7b59d39.jpg",
      "https://i.pinimg.com/236x/70/66/ae/7066aedc89495c1ba1a777ba50a62697.jpg",
    ],),
  ListCast( namecast: [
    "Jason Momoa",
    "Sylvia Hoeks",
    "Archie Madekwe",
    "Hera Hilmar",
  ],
    imgcast: [
      "https://i.pinimg.com/236x/f5/d9/43/f5d9435f91ca44004c24f338cf186136.jpg",
      "https://i.pinimg.com/236x/4d/66/44/4d6644bc9aa3bfc372b98181285c7545.jpg",
      "https://i.pinimg.com/236x/8f/04/c7/8f04c7404ca0d95d62aabce00a6fee29.jpg",
      "https://i.pinimg.com/564x/25/a8/b5/25a8b531e7e2eef815821a1f09d62e23.jpg",
    ],),
  ListCast( namecast: [
    "Ezra Miller",
    "Michael Keaton",
    "Sasha Calle",
    "Michael Shannon",
  ],
    imgcast: [
      "https://i.pinimg.com/236x/42/1a/f5/421af5d6ceb9cd181e6f1d9e59b56cc1.jpg",
      "https://i.pinimg.com/564x/b2/c3/f1/b2c3f101ae798caca1b750003b076bcc.jpg",
      "https://i.pinimg.com/236x/f0/3d/88/f03d885b892f2a91f71e67ef41740c44.jpg",
      "https://i.pinimg.com/236x/fe/0d/92/fe0d920d1ab54cbe9f54463d403cb5f0.jpg",
    ],),
];

class MovieList {
  String moviename;
  String date;
  String lenght;
  String imageurl;
  List<ListCast> listcast;
  MovieList(
      {required this.moviename,
      required this.date,
      required this.imageurl,
        required this.lenght,
      required this.listcast});
}

List<MovieList> storylist = [
  MovieList(
      moviename: "DareDevil",
      date: "21 December 2012",
      imageurl: "assets/images/daredevill.jpg",
      lenght: "13 Episode",
      listcast: []),
  MovieList(
      moviename: "Ghost Rider",
      date: "February 16, 2007",
      imageurl: "assets/images/ghostrider.jpg",
      lenght: "1h 54m",
      listcast: []),
  MovieList(
      moviename: "See",
      date: "November 1, 2019",
      imageurl: "assets/images/see.jpg",
      lenght: "24 Episode",
      listcast: []),
  MovieList(
      moviename: "The Flash",
      date: "June 16, 2023",
      imageurl: "assets/images/theflash.jpg",
      lenght: "2h 24m",
      listcast: []),
];
