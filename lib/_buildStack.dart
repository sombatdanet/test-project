import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_omkousol/List.dart';
class ErroStack extends StatefulWidget {
  final NetFlix movieModel;
  ErroStack({required this.movieModel});
  @override
  State<ErroStack> createState() => _ErroStackState();
}

class _ErroStackState extends State<ErroStack> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          _buildImage(widget.movieModel.url),
          _buildShadow(
              widget.movieModel.title, widget.movieModel.subtitle),
        ],
      ),
    );
  }

  Widget _buildShadow(String mText, String subtile) {
    return Container(
      height: 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              "${mText}",
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 5),
            ),
          ),
          Text(
            "${subtile}",
            style: TextStyle(
                fontSize: 10, color: Colors.white, fontWeight: FontWeight.w500),
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.black.withOpacity(0.1),
                  Colors.black.withOpacity(0.5),
                ])),
          ),
          _buildRow
        ],
      ),
    );
  }

  Widget get _buildRow {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
        Container(
          height: 50,
          child: Column(
            children: [
              Icon(
                Icons.check,
                color: Colors.white,
              ),
              Text("My List", style: TextStyle(color: Colors.white))
            ],
          ),
        ),
        ElevatedButton.icon(
          icon: Icon(Icons.play_arrow),
          onPressed: () {},
          label: Text("Play"),
          style: ElevatedButton.styleFrom(backgroundColor: Colors.purple[900]),
        ),
        Container(
          height: 50,
          child: Column(
            children: [
              Icon(Icons.info_outline, color: Colors.white),
              Text("info", style: TextStyle(color: Colors.white))
            ],
          ),
        ),
      ]),
    );
  }
  Widget _buildImage(String url){
    return    Container(
      child:CachedNetworkImage(
        imageUrl: url,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter:
                ColorFilter.mode(Colors.red.withOpacity(0.1), BlendMode.colorBurn)),
          ),
        ),
        placeholder: (context, url) => Center(child: CircularProgressIndicator()),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }
}
