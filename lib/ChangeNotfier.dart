
import 'package:flutter/cupertino.dart';
import 'package:flutter_omkousol/List.dart';


class TextChange extends ChangeNotifier{
  Map<int,NetFlix> netflix = Map();

  int _count =0;
  int get count =>_count;

  get text => null;

  bool _onTap = true;
  bool get onTap =>_onTap;
  void increas(){
    _count++;
    notifyListeners();
  }
  List<NetFlix> _listSave = [];
  List<NetFlix> get listSave => _listSave;
  void addFavorite(NetFlix save){
    _listSave.add(save);
    notifyListeners();
  }
  bool isFavorite(NetFlix save){
    return  _listSave.contains(save);
  }
  void removeFavorite(NetFlix save){
    _listSave.remove(save);notifyListeners();
  }
}