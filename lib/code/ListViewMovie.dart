import 'package:flutter/material.dart';
import 'package:flutter_omkousol/List.dart';

class ListViewMovie extends StatelessWidget {
  final MovieList storylist;
  const ListViewMovie({super.key, required this.storylist});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white70,
      ),
      margin: EdgeInsets.only(top: 10,left: 10,right: 10),
      height:100,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            height: 80,
            width: 60,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              image: DecorationImage(
                image: AssetImage(storylist.imageurl),
                fit: BoxFit.fill
              )
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(storylist.moviename),
                Text(storylist.date),
              ],
            ),
          )
        ],
      ),
    );
  }
}
