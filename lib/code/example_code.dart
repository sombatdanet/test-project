class MyApp extends StatefulWidget {
  final List<MovieList> storylist;
  const MyApp({required this.storylist});
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>  {
  late Timer timer;
  final PageController _pageController = PageController();
  Listtiel listtiel = Listtiel();
  final PageController _controller = PageController();

  int index = 0;
  void chagePage(int index){
    _newPagecontroller.jumpToPage(index);
  }
  void initState() {
    timer = Timer.periodic(Duration(seconds: 3), (timer) {
      index++;
      if (index == widget.netflix.length) {
        setState(() {
          index = 0;
        });
      }
      _pageController.animateToPage(index,
          duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer.cancel();
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home:Homepage(storymovie: storylist,),
    );
  }
  PageController _newPagecontroller = PageController();
  Widget get _buidlPageView{
    return PageView(
        controller: _newPagecontroller,
        onPageChanged: (int index){
          _cindex=index;
        },
        children:[
          _buildNetflix(context),
          _buildScrolls(context),
          listtiel,
        ]
    );
  }
  Widget _buildNetflix(BuildContext context) {
    return Container(
        child: ListView(children: [
          _buildScrolls(context),
          _buildScroll(context),
        ]));
  }

  Widget get _buildText {
    return Container(
      padding: EdgeInsets.only(left: 10, top: 10),
      color: Colors.black,
      child: Text(
        "Continue Watching for Danet",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 15),
      ),
    );
  }

  Widget _buildScroll(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 1, left: 20),
        height: 180,
        color: Colors.black,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.netflix.length,
          itemBuilder: (BuildContext context, index) {
            return Movienew(movie: widget.movie[index], netflix: widget.netflix[index],);
          },
        ));
  }

  int _cindex = 0;
  Widget get _build {
    return BottomNavigationBar(
        unselectedFontSize: 0.0,
        selectedFontSize: 0.0,
        currentIndex: _cindex,
        onTap:(int index){
          chagePage(index);
          setState(() {
            _cindex=index;
          });
        },
        selectedItemColor:Colors.pink,
        unselectedItemColor:Colors.grey,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home, ),label:"Home" ),
          BottomNavigationBarItem(icon: Icon(Icons.settings,),label:"setting"),
          BottomNavigationBarItem(icon: Icon(Icons.list,),label:"List"),
        ]);
  }

  Widget _buildScrolls(BuildContext context) {
    return Container(
        height: 500,
        color: Colors.black,
        child: PageView.builder(
          controller: _pageController,
          scrollDirection: Axis.horizontal,
          itemCount: widget.netflix.length,
          itemBuilder: (BuildContext context, index) {
            return ErroStack(movieModel: widget.netflix[index]);
          },
        ));
  }
}


